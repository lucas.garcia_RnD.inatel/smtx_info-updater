'''DB_Converter v1


'''
import os
import platform
from Connections import conecta_mariadb
import pandas as pd
from datetime import datetime

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #definindo diretorios
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))

    # conectando ao NPsmart
    all_tables = os.listdir(csv_path)
    
    # criando cursor do bd
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()


    print(f'>>>>>> START DATABASE UPLOAD <<<<<<<')
    start = datetime.now()

    for current_table in all_tables:
        now = datetime.now().strftime("%Y-%m-%d")
        #definindo parametros
        table = current_table.replace('.xlsx', '')
        current_mo_dir = f'{csv_path}\\{current_table}'
        df_csv = pd.read_excel(current_mo_dir, dtype='str')
        df_csv.insert(2,'DATE', f'{now}', allow_duplicates=True)
        df_csv.dropna(subset=['Status SMTX'], inplace=True)
        csv_file = os.path.abspath(os.path.join(directory_path, 'csv', f'NE_SMTX_{now}.csv'))
        csv_done_file = os.path.abspath(os.path.join(directory_path, 'done', f'NE_SMTX_{now}.csv'))
        df_csv.to_csv (csv_file, index = None, header=False)
        os.remove(current_mo_dir)

        #criando tabela se o comando for novo
        list_colums = list(df_csv.columns)
        sql_table_creation = f'CREATE TABLE IF NOT EXISTS tx_ne_param.NE_SMTX ('
        for column in list_colums:
            if not column == 'DATE':
                column = column.replace('.','')
                column = column.replace('/','_')
                column = column.replace('-','_')
                sql_insert_col = f"{column.replace(' ','_')} varchar(100) DEFAULT '-', "
                sql_table_creation = sql_table_creation + sql_insert_col
            else:
                column = column.replace('.','')
                column = column.replace('-','_')
                sql_insert_col = f"{column.replace(' ','_')} date DEFAULT NULL, "
                sql_table_creation = sql_table_creation + sql_insert_col

        sql_table_creation = sql_table_creation[:-2] + ');'
        cursor.execute(sql_table_creation)
        con_bacos.commit()

        #limpando tabela (DANGER ------ UTILIZAR SÓ SE FOR REALMENTE NECESSÁRIO)
        table_clean = f'TRUNCATE tx_ne_param.NE_SMTX;'
        cursor.execute(table_clean)
        con_bacos.commit()

        #subindo CSV na tabela
        # csv_final_path = os.path.join(csv_path, current_table)
        upload_csv_path = csv_file.replace('\\', '/')
        cur_platform = platform.system()
        print(f'Loading NE_SMTX...')
        if cur_platform.upper() == 'WINDOWS':
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE tx_ne_param.NE_SMTX\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
        else:
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE tx_ne_param.NE_SMTX\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
        cursor.execute(sql)
        con_bacos.commit()

        print(f'Done: NE_SMTX')
        os.rename(csv_file, csv_done_file)

    con_bacos.close()
    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'Uploaded in {delta}.')
    print(f'>>>>>> FINISH DATABASE UPLOAD <<<<<<<')