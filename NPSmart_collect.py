'''DB_Converter v1

Utilitario utilizado para conectar ao NPSmart e coletar as tabelas selecionada para importar ao BD BACOS mariaDb

1 - conectar no SPES e rodar o NPSMART_COLLECT (play na linha 15).
2 - após finalizado conectar nas duas openconnect e rodar o BACOS_UPLOADER (play na linha 9).

'''
from datetime import datetime
import os
import pandas as pd
from Connections import conecta_postgres

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #definindo diretorios
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))

    #Flag para limpar CSVs velhos
    CleanCSV = True

    #loop lista e deleta csv
    if CleanCSV:
        current_csv = [f for f in os.listdir(csv_path)]
        for cur_csv in current_csv:
            os.remove(os.path.join(csv_path, cur_csv))

    #table dict {table:from_schema}
    table_dict = {'enodebcell_lte':'huawei_configuration',
                  'enodebfunction_sran':'huawei_configuration',
                  'cellop_sran':'huawei_configuration',
                  'cnoperatorta_sran':'huawei_configuration',
                  'ucell_rnc_wcdma':'huawei_configuration',
                  'gcell_gsm':'huawei_configuration',
                  'twampresponder_lte':'huawei_configuration',
                  'twampresponder_sran':'huawei_configuration',
                  'enodeb_lte':'huawei_configuration',
                  'ne_lte':'huawei_configuration',
                  's1_lte':'huawei_configuration',
                  's1_sran':'huawei_configuration',
                  'epgroup_lte':'huawei_configuration',
                  'epgroup_sran':'huawei_configuration',
                  'enodebsctphost_lte':'huawei_configuration',
                  'sctphost_sran':'huawei_configuration',
                  'enodebuserplanehost_lte':'huawei_configuration',
                  'userplanehost_sran':'huawei_configuration',
                  'sctplnk_rnc_wcdma':'huawei_configuration',
                  'sctplnk_nodeb_wcdma':'huawei_configuration',
                  'vw_reassembly_daily_lte':'huawei_kpi'}

    #gerando conexão com npsmartDB
    con_npsmart = conecta_postgres()

    print(f'>>>>>> START DATABASE DOWNLOAD <<<<<<<')
    start = datetime.now()

    #Loop pelo dicionario de tabelas baixando e salvando csv local
    for table in table_dict:
        print(f'Working on table {table}:')
        csv_df = pd.read_sql(f'SELECT * FROM {table_dict[table]}.{table}', con=con_npsmart)
        csv_file = os.path.abspath(os.path.join(csv_path, f'{table}.csv'))
        csv_df.to_csv(csv_file, mode='w', header=True, index=False)
        print(f'Saving {table}...')


    con_npsmart.close()
    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'Downloaded in {delta}.')
    print(f'>>>>>> FINISH DATABASE DOWNLOAD <<<<<<<')

