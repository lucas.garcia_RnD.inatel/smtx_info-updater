'''
Funções para gerar conexão com os respectivos DBs
'''
import psycopg2
import mysql.connector as mysql

def conecta_postgres():
    con_postgres = psycopg2.connect(host='7.184.73.78',
                                    database='npsmart',
                                    user='postgres',
                                    password='NPSmart#10')
    return con_postgres


def conecta_mariadb():
    con_mariadb = mysql.connect(
        host="10.26.82.160",
        user='lrgsouza',
        passwd='luc4S2020',
        allow_local_infile=True
    )
    return con_mariadb
